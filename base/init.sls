include:
    - base.avahi
    - base.journald
    - base.localgit
    - base.mnt
    - base.sound
    - base.sources
    - base.syslog
    - base.timesyncd
    - base.tsocks
